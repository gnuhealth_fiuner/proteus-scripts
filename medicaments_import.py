#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import csv
#from datetime import datetime
import sys
from proteus import Model
from proteus import config as pconfig
from optparse import OptionParser
from decimal import Decimal
from trytond.transaction import Transaction

import random


def main(options):
    config = pconfig.set_trytond(options.database, options.admin, config_file=options.config_file)
    Lang = Model.get('ir.lang')
    (es,) = Lang.find([('code', '=', 'es')])
    es.translatable = True
    es.save()

    create_patients(config, es, options)
    print ("done.")

def create_patients(config, lang, options):
    ProductTemplate = Model.get('product.template')
    Product = Model.get('product.product')
    Medicament = Model.get('gnuhealth.medicament')
    DoseUnit = Model.get('gnuhealth.dose.unit')
    UOM = Model.get('product.uom')
    CATEGORY = Model.get('product.category')
    Form = Model.get('gnuhealth.drug.form')
    context = {'language':'en'}

    uom = UOM.find([('name','=','Unit')])

    with open(options.file_csv,encoding="utf-8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")

        next(csv_reader) #skip header
        count = 0
        for line in csv_reader:
            count +=1
            '''Put all row data into variables'''
            name = line[0]
            try:
                strength = float(line[1].replace(',', '.')) if line[1] != '' else None
            except:
                strengh = None
            type_ = 'goods'
            is_medicament = True
            dose_unit = line[2] if line[2] != '' else None
            presentacion = line[3] if line[3] != '' else None

            '''search if already exists'''
            template = ProductTemplate.find(['name','=',name])
            if len(template) == 0:
                template = ProductTemplate(
                        name = name,
                        type = type_,
                        default_uom = uom[0].id,
                        list_price = Decimal(0),
                        )
                template.save()
            else:
                template = template[0]
            product = template.products[0]
            product.is_medicament = True
            product.save()
            print(product.rec_name)
            print(strength, type(strength))
            medicament = Medicament.find([('name','=',product.id)])
            if not len(medicament):
                unit = None
                if dose_unit:
                    unit= DoseUnit.find([
                        ('name', '=', dose_unit)])
                    if unit:
                        unit= unit[0]
                    else:
                        unit = None
                form = None
                if presentacion:
                    form = Form.find([
                        ('name', '=', presentacion)
                        ])
                    if form:
                        form = form[0]
                    else:
                        form = None
                print(form)
                medicament = Medicament()
                medicament.name = product
                medicament.strength = strength
                medicament.unit = unit
                medicament.form = form
                medicament.incluir_salud = True
                medicament.save()

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--database', dest='database')
    parser.add_option('-c', '--config-file', dest='config_file')
    parser.add_option('-a', '--admin', dest='admin')
    parser.add_option('-f','--file',dest='file_csv')
    parser.set_defaults(user='admin')

    options, args = parser.parse_args()
    if args:
        parser.error('Parametros incorrectos')
    if not options.database:
        parser.error('Se debe definir [nombre] de base de datos')
    if not options.config_file:
        parser.error('Se debe definir el path absoluto al archivo de '
            'configuracion de trytond')
    if not options.admin:
        parser.error('Se debe definir el usuario admin')
    if not options.file_csv:
        parser.error('Se debe especificar el archivo csv')
    
    main(options)
   
