import sys
import string
import os

import sys
from proteus import Model
from proteus import config as pconfig
from optparse import OptionParser

#usage
#python3 index_url_patient.py -a admin -d DATA_BASE -c trytond.conf -i id -m max_id -s step

def main(options):
    config = pconfig.set_trytond(options.database, options.admin, config_file=options.config_file)
    Lang = Model.get('ir.lang')
    (es,) = Lang.find([('code', '=', 'es')])
    es.translatable = True
    es.save()

    attach_files(config, es, options)
    print ("done.")

def attach_files(config, es, options):
    Patient = Model.get('gnuhealth.patient')
    Attachment = Model.get('ir.attachment')

    counter = 0
    id_ = int(options.id_) - 1
    step = int(options.step) - 1
    max_id = int(options.max_id)
    attachments = []
    counter = 0
    while not attachments and counter <= max_id:
        patients = Patient.find([('id', '>', str(id_))],
                limit=step)
        patient_attachments_lower = [
                ''.join([patient.puid, ' ', patient.gender.lower(), '.pdf'])
                for patient in patients if patient.gender and patient.puid]
        patient_attachments_upper = [
                ''.join([patient.puid, ' ', patient.gender.upper(), '.pdf'])
                for patient in patients if patient.gender and patient.puid]
        patient_attachments = patient_attachments_lower + patient_attachments_upper
        attachments = Attachment.find([
            ('type', '=', 'data'),
            ('name', 'in', patient_attachments)
            ])
        if attachments:
            print (attachments)
        counter = id_
        id_ += step

    attach_path = "/home/gnuhealth/attach/health"

    if attachments:
        files_id = [attach.file_id for attach in attachments]
        print(files_id)
        for dirpath, dirnames, filenames in os.walk(attach_path):
            for file_ in filenames:
                file_complete = os.path.join(dirpath, file_)
                if file_ in files_id:
                    print('inside \'if file_ in files_id\'')
                    try:
                        os.remove(file_complete)
                        print(f"File removed: {file_complete}")
                    except Exception as e:
                        print(f"Couldn\'t remove {file_complete}: {str(e)}")
        Attachment.delete(attachments)

    # clean orphans (those files wich are not related to any register)
    all_attachments = attachments = Attachment.find([
                            ('type', '=', 'data'),
                            ])
    files_id = [attach.file_id for attach in all_attachments]
    for dirpath, dirnames, filenames in os.walk(attach_path):
        for file_ in filenames:
            file_complete = os.path.join(dirpath, file_)
            if file_ not in files_id:
                try:
                    os.remove(file_complete)
                    print(f"File removed: {file_complete}")
                except Exception as e:
                    print(f"Couldn\'t remove {file_complete}: {str(e)}")


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--database', dest='database')
    parser.add_option('-c', '--config-file', dest='config_file')
    parser.add_option('-a', '--admin', dest='admin')
    parser.add_option('-i', '--id', dest='id_')
    parser.add_option('-m', '--max', dest='max_id')
    parser.add_option('-s', '--step', dest='step')
    parser.set_defaults(user='admin')

    options, args = parser.parse_args()
    if args:
        parser.error('Parametros incorrectos')
    if not options.database:
        parser.error('Se debe definir [nombre] de base de datos')
    if not options.config_file:
        parser.error('Se debe definir el path absoluto al archivo de '
            'configuracion de trytond')
    if not options.admin:
        parser.error('Se debe definir el usuario admin')
    if not options.id_:
        parser.error('Se debe definir el id de donde comenzar')
    if not options.max_id:
        parser.error('Se debe definir el max_id donde terminar')
    if not options.step:
        parser.error('Se debe definir la cantidad de repeticiones de busqueda')
    main(options)
